#include "Circle.h"
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <iostream>

Circle::Circle()
{
}



double Circle::getX() { 
	return this->mX; 
}
double Circle::getY() {
	return this->mY; 
}
double Circle::getRadius() { 
	return this->mRadius; 
}
double Circle::getXSpeed() { 
	return this->mXspeed; 
}
double Circle::getYSpeed() { 
	return this->mYspeed; 
}
double Circle::getRed() { 
	return this->mRed; 
}
double Circle::getGreen() { 
	return this->mGreen; 
}
double Circle::getBlue() { 
	return this->mBlue;
}
double Circle::getNextX() { 
	return this->mX + this->mXspeed;
}
double Circle::getNextY() { 
	return this->mY + this->mYspeed;
}

void Circle::setX(double val) { 
	this->mX = val; 
}
void Circle::setY(double val) { 
	this->mY = val; 
}
void Circle::setRadius(double val) { 
	this->mRadius = val;
}
void Circle::setXSpeed(double val) { 
	this->mXspeed = val;
}
void Circle::setYSpeed(double val) { this->mYspeed = val; }

void Circle::setRed(double val) { 
	this->mRed = val; 
}
void Circle::setGreen(double val) { 
	this->mGreen = val; 
}
void Circle::setBlue(double val) { 
	this->mBlue = val; 
}
