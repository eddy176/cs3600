#include <cstdlib>
#include <vector>
#include <random>
#include "glut.h"
#include "graphics.h"
#include "maze.h"
#include <time.h>

Cell::Cell()
{
	left = top = right = bottom = true;
	visited = false;
}
void Cell::Draw(int x, int y)
{
	glColor3d(0, 0, 0);
	if (left)
		DrawLine(x, y, x, y + 1);
	if (top)
		DrawLine(x, y + 1, x + 1, y + 1);
	if (right)
		DrawLine(x + 1, y + 1, x + 1, y);
	if (bottom)
		DrawLine(x + 1, y, x, y);
}

Maze::Maze()
{
	srand(time(NULL));

	//Random start and end
	mStart = rand() % WIDTH;
	mEnd = rand() % WIDTH;

}
bool Maze::isSafe(double x, double y, double radius)
{
	int i = (int)x;
	int j = (int)y;
	double xoffset = x - i;
	double yoffset = y - j;

	//wall collisions
	if (cells[i][j].right && xoffset + radius > 1.0) //not safe right
		return false;
	if (cells[i][j].left && xoffset - radius < 0.0) //not safe left
		return false;
	if (cells[i][j].top && yoffset + radius > 1.0) //not safe top
		return false;
	if (cells[i][j].bottom && yoffset - radius < 0.0) //not safe bottom
		return false;
	
	//corner collision
	if (xoffset + radius > 1 && yoffset + radius > 1) //top right
		return false;
	if (xoffset + radius > 1 && yoffset - radius < 0) //bottom right
		return false;
	if (xoffset + radius < 0 && yoffset + radius > 1) //top left
		return false;
	if (xoffset + radius < 0 && yoffset - radius < 0) //bottom left
		return false;
		
	return true;  // not inside a wall or cworner

}

void Maze::RemoveWalls()
{
	srand(time(NULL));

	RemoveWallsR(0, 0);
	cells[mEnd][HEIGHT - 1].top = false;
}

void Maze::RemoveWallsR(int i, int j)
{
	cells[i][j].visited = true;

	while (true)
	{
		// record legal possibilitites between LEFT, UP, RIGHT, DOWN
		enum MOVES { LEFT, UP, RIGHT, DOWN };
		std::vector<MOVES> moves;

		// check for a legal LEFT move
		if (i - 1 >= 0 && !cells[i - 1][j].visited)
		{
			moves.push_back(LEFT);
		}

		// check other 3 directions
		// check for a legal RIGHT move
		if (i + 1 < WIDTH && !cells[i + 1][j].visited)
		{
			moves.push_back(RIGHT);
		}
		// check for a legal DOWN move
		if (j - 1 >= 0 && !cells[i][j - 1].visited)
		{
			moves.push_back(DOWN);
		}
		// check for a legal UP move
		if (j + 1 < HEIGHT && !cells[i][j + 1].visited)
		{
			moves.push_back(UP);
		}
		if (moves.size() == 0)
		{
			return;
		}

		// pick which direction randomly
		int r = rand() % moves.size();

		if (moves[r] == LEFT) {

			cells[i][j].left = false;
			cells[i - 1][j].right = false;
			RemoveWallsR(i - 1, j);
		}

		// Likewise for other 3 directions
		if (moves[r] == RIGHT)
		{
			cells[i][j].right = false;
			cells[i + 1][j].left = false;
			RemoveWallsR(i + 1, j);

		}
		if (moves[r] == UP)
		{
			cells[i][j].top = false;
			cells[i][j + 1].bottom = false;
			RemoveWallsR(i, j + 1);
		}
		if (moves[r] == DOWN)
		{
			cells[i][j].bottom = false;
			cells[i][j - 1].top = false;
			RemoveWallsR(i, j - 1);
		}

	}

}
void Maze::Draw()
{
	for (int i = 0; i < WIDTH; i++)
		for (int j = 0; j < HEIGHT; j++)
			cells[i][j].Draw(i, j);
}
