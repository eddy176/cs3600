class Mouse
{
public:
	Mouse();
	void Draw();
	void drawCube(double size);
	void DrawRectangle(double x1, double y1, double x2, double y2);
	void spinLeft();
	void spinRight();
	void moveForward();
	void moveBack();
	void moveToStart();
	void moveToEnd();
	void moveToRandom();
	double getX();
	double getY();
	double getSize();
	double getDirection();
	double getXSpeed();
	double getYSpeed();
private:
	double direction;  // in degrees
	double x, y;  // in cell coords
	double xSpeed, ySpeed;
	double size;
};
