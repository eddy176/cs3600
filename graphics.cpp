// OpenGL/GLUT starter kit for Windows 7 and Visual Studio 2010
// Created spring, 2011
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to an empty Windows Console project
//		and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.
// OR, see GlutDirectories.txt for a better place to put them.

#include <cmath>
#include <ctime>
#include <cstring>
#include <random> 
#include <vector>
#include <iostream>
#include "Circle.h"
#include "glut.h"

// Global Variables (Only what you need!)
double screen_x = 700;
double screen_y = 500;
std::vector<Circle> gCircles;


// 
// Functions that draw basic primitives
//
void DrawCircle(double x1, double y1, double radius)
{
	glBegin(GL_POLYGON);
	for(int i=0; i<32; i++)
	{
		double theta = (double)i/32.0 * 2.0 * 3.1415926;
		double x = x1 + radius * cos(theta);
		double y = y1 + radius * sin(theta);
		glVertex2d(x, y);
	}
	glEnd();
}

void DrawRectangle(double x1, double y1, double x2, double y2)
{
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y1);
	glVertex2d(x2,y2);
	glVertex2d(x1,y2);
	glEnd();
}

void DrawTriangle(double x1, double y1, double x2, double y2, double x3, double y3)
{
	glBegin(GL_TRIANGLES);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glEnd();
}

// Outputs a string of text at the specified location.
void DrawText(double x, double y, const char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
	
	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

    glDisable(GL_BLEND);
}


//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	// Test lines that draw all three shapes and some text.
	// Delete these when you get your code working.
	glColor3d(0,0,1);
	// circle-wall collisions
	for (size_t i = 0; i < gCircles.size(); i++)
	{
		if (gCircles[i].getNextY() + gCircles[i].getRadius()>= screen_y)
		{
			double speed = -abs(gCircles[i].getYSpeed());
			gCircles[i].setYSpeed(speed);

		}

		if (gCircles[i].getNextY() - gCircles[i].getRadius() < 0)
		{
			double speed = abs(gCircles[i].getYSpeed());
			gCircles[i].setYSpeed(speed);
		}

		if (gCircles[i].getNextX() - gCircles[i].getRadius() < 0)
		{
			double speed = abs(gCircles[i].getXSpeed());
			gCircles[i].setXSpeed(speed);
		}

		if (gCircles[i].getNextX() + gCircles[i].getRadius() >= screen_x)
		{
			double speed = -abs(gCircles[i].getXSpeed());
			gCircles[i].setXSpeed(speed);
		}
	}
	// move
	for (size_t i = 0; i < gCircles.size(); i++)
	{
		
		gCircles[i].setX(gCircles[i].getNextX());
		gCircles[i].setY(gCircles[i].getNextY());
	}
	
	// draw
	for (size_t i = 0; i < gCircles.size(); i++)
	{
		glColor3d(gCircles[i].getRed(), gCircles[i].getGreen(), gCircles[i].getBlue());
		DrawCircle(gCircles[i].getX(), gCircles[i].getY(), gCircles[i].getRadius());
	}

	glutSwapBuffers();

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
		case 27: // escape character means to quit the program
			exit(0);
			break;
		case 'b':
			// do something when 'b' character is hit.
			break;
		default:
			return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	glutPostRedisplay();
}

double random(double min, double max)
{
	double num = 0;

	while (num == 0)
	{
		num = (max - min) * ((double)rand() / (double)RAND_MAX) + min;
	}

	return num;
}

// Your initialization code goes here.
void InitializeMyStuff()
{
	srand(time(NULL));

    for (int i = 0; i <= 10; i++)
    {
        Circle c;
		double rad = random(15, 60);
		double x = random(c.getRadius(), screen_x - c.getRadius());
		double y = random(c.getRadius(), screen_y - c.getRadius());
		double xs = random(0, 1);
		double ys = random(0, 1);
		double r = random(0, 1);
		double g = random(0, 1);
		double b = random(0, 1);
		c.setRadius(rad);
		c.setX(x);
		c.setY(y);
		c.setXSpeed(xs);
		c.setYSpeed(ys);
		c.setRed(r);
		c.setBlue(b);
		c.setGreen(g);

        gCircles.push_back(c);
    }

}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Edwins circles");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);

	glColor3d(0,0,0); // forground color
	glClearColor(1, 1, 1, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
