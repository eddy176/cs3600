// (Edwin Manjarrez)
// Chess animation starter kit.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <ctime>
using namespace std;
#include "piece.h"
#include "glut.h"
#include "graphics.h"


// Global Variables
// Some colors you can use, or make your own and add them
// here and in graphics.h
GLfloat redMaterial[] = { 0.7, 0.1, 0.2, 1.0 };
GLfloat greenMaterial[] = { 0.1, 0.7, 0.4, 1.0 };
GLfloat brightGreenMaterial[] = { 0.1, 0.9, 0.1, 1.0 };
GLfloat blueMaterial[] = { 0.1, 0.2, 0.7, 1.0 };
GLfloat whiteMaterial[] = { 3.0, 3.0, 3.0, 1.0 };

GLfloat white[] = { 0.9, 0.9, 0.9, 1.0 };
GLfloat grey[] = { 0.15, 0.15, 0.15, 1.0 };
GLfloat lightGrey[] = { 0.6, 0.6, 0.6, 1.0 };
GLfloat darkGrey[] = { 0.4, 0.4, 0.4, 1.0 };

GLfloat kingX = 4000;
GLfloat kingY = 0;
GLfloat kingZ = 1000;


Piece kingW(kingX, kingY, kingZ);



double screen_x = 600;
double screen_y = 500;


double GetTime()
{
	static clock_t start_time = clock();
	clock_t current_time = clock();
	double total_time = double(current_time - start_time) / CLOCKS_PER_SEC;
	return total_time;
}

// Outputs a string of text at the specified location.
void text_output(double x, double y, const char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
	
	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

    glDisable(GL_BLEND);
}

// Given the three triangle points x[0],y[0],z[0],
//		x[1],y[1],z[1], and x[2],y[2],z[2],
//		Finds the normal vector n[0], n[1], n[2].
void FindTriangleNormal(double x[], double y[], double z[], double n[])
{
	// Convert the 3 input points to 2 vectors, v1 and v2.
	double v1[3], v2[3];
	v1[0] = x[1] - x[0];
	v1[1] = y[1] - y[0];
	v1[2] = z[1] - z[0];
	v2[0] = x[2] - x[0];
	v2[1] = y[2] - y[0];
	v2[2] = z[2] - z[0];
	
	// Take the cross product of v1 and v2, to find the vector perpendicular to both.
	n[0] = v1[1]*v2[2] - v1[2]*v2[1];
	n[1] = -(v1[0]*v2[2] - v1[2]*v2[0]);
	n[2] = v1[0]*v2[1] - v1[1]*v2[0];

	double size = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
	n[0] /= -size;
	n[1] /= -size;
	n[2] /= -size;
}

// Loads the given data file and draws it at its default position.
// Call glTranslate before calling this to get it in the right place.
void DrawPiece(const char filename[])
{
	// Try to open the given file.
	char buffer[200];
	ifstream in(filename);
	if(!in)
	{
		cerr << "Error. Could not open " << filename << endl;
		exit(1);
	}

	double x[100], y[100], z[100]; // stores a single polygon up to 100 vertices.
	int done = false;
	int verts = 0; // vertices in the current polygon
	int polygons = 0; // total polygons in this file.
	do
	{
		in.getline(buffer, 200); // get one line (point) from the file.
		int count = sscanf_s(buffer, "%lf, %lf, %lf", &(x[verts]), &(y[verts]), &(z[verts]));
		done = in.eof();
		if(!done)
		{
			if(count == 3) // if this line had an x,y,z point.
			{
				verts++;
			}
			else // the line was empty. Finish current polygon and start a new one.
			{
				if(verts>=3)
				{
					glBegin(GL_POLYGON);
					double n[3];
					FindTriangleNormal(x, y, z, n);
					glNormal3dv(n);
					for(int i=0; i<verts; i++)
					{
						glVertex3d(x[i], y[i], z[i]);
					}
					glEnd(); // end previous polygon
					polygons++;
					verts = 0;
				}
			}
		}
	}
	while(!done);

	if(verts>0)
	{
		cerr << "Error. Extra vertices in file " << filename << endl;
		exit(1);
	}

}

// NOTE: Y is the UP direction for the chess pieces.
double eye[3] = {7500, 9000, -5000}; // pick a nice vantage point.
double at[3]  = {4500, 0,     4000};
//
// GLUT callback functions
//

// As t goes from t0 to t1, set v between v0 and v1 accordingly.
void Interpolate(double t, double t0, double t1,
	double & v, double v0, double v1)
{
	double ratio = (t - t0) / (t1 - t0);
	if (ratio < 0)
		ratio = 0;
	if (ratio > 1)
		ratio = 1;
	v = v0 + (v1 - v0)*ratio;
}



Piece rookLeftW(1000, 0, 1000);
Piece knightLeftW(2000, 0, 1000);
Piece bishopLeftW(3000, 0, 1000);
Piece queenW(5000, 0, 1000);
Piece bishopRightW(6000, 0, 1000);
Piece knightRightW(7000, 0, 1000);
Piece rookRightW(8000, 0, 1000);
Piece pawn1W(1000, 0, 2000);
Piece pawn2W(2000, 0, 2000);
Piece pawn3W(3000, 0, 2000);
Piece pawn4W(4000, 0, 2000);
Piece pawn5W(5000, 0, 2000);
Piece pawn6W(6000, 0, 2000);
Piece pawn7W(7000, 0, 2000);
Piece pawn8W(8000, 0, 2000);

Piece rookLeftB(1000, 0, 8000);
Piece knightLeftB(2000, 0, 8000);
Piece bishopLeftB(3000, 0, 8000);
Piece kingB(4000, 0, 8000);
Piece queenB(5000, 0, 8000);
Piece bishopRightB(6000, 0, 8000);
Piece knightRightB(7000, 0, 8000);
Piece rookRightB(8000, 0, 8000);
Piece pawn1B(1000, 0, 7000);
Piece pawn2B(2000, 0, 7000);
Piece pawn3B(3000, 0, 7000);
Piece pawn4B(4000, 0, 7000);
Piece pawn5B(5000, 0, 7000);
Piece pawn6B(6000, 0, 7000);
Piece pawn7B(7000, 0, 7000);
Piece pawn8B(8000, 0, 7000);

enum piece_numbers { pawn = 100, king, queen, rook, bishop, knight };


void drawBlackPawns(double thisTime)
{

	glPushMatrix();
	glTranslatef(pawn1B.x, 0, pawn1B.z);
	glCallList(pawn);
	glPopMatrix();

	Interpolate(thisTime, 40, 60, pawn2B.x, pawn2B.x, 2000);
	Interpolate(thisTime, 40, 60, pawn2B.z, pawn2B.z, 5000);
	glPushMatrix();
	glTranslatef(pawn2B.x, 0, pawn2B.z);
	glCallList(pawn);
	glPopMatrix();


	Interpolate(thisTime, 215, 225, pawn3B.x, pawn3B.x, pawn3B.x);
	Interpolate(thisTime, 215, 225, pawn3B.z, pawn3B.z, 12000);
	glPushMatrix();
	glTranslatef(pawn3B.x, 0, pawn3B.z);
	double deg = 0;
	Interpolate(thisTime, 215, 220, deg, 0, -90);
	glRotated(deg, -90, 0, 1);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn4B.x, 0, pawn4B.z);
	glCallList(pawn);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(pawn5B.x, 0, pawn5B.z);
	glCallList(pawn);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(pawn6B.x, 0, pawn6B.z);
	glCallList(pawn);
	glPopMatrix();

	Interpolate(thisTime, 170, 190, pawn7B.x, pawn7B.x, 7000);
	Interpolate(thisTime, 170, 190, pawn7B.z, pawn7B.z, 5000);
	glPushMatrix();
	glTranslatef(pawn7B.x, 0, pawn7B.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn8B.x, 0, pawn8B.z);
	glCallList(pawn);
	glPopMatrix();
}

void drawWhitePawns(double thisTime)
{

	glPushMatrix();
	glTranslatef(pawn1W.x, 0, pawn1W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn2W.x, 0, pawn2W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn3W.x, 0, pawn3W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn4W.x, 0, pawn4W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn5W.x, 0, pawn5W.z);
	glCallList(pawn);
	glPopMatrix();

	Interpolate(thisTime, 10, 30, pawn4W.z, pawn4W.z, 6000);
	Interpolate(thisTime, 10, 30, pawn4W.x, pawn4W.x, 6000);
	glPushMatrix();
	glTranslatef(pawn6W.x, 0, pawn6W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn7W.x, 0, pawn7W.z);
	glCallList(pawn);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(pawn8W.x, 0, pawn8W.z);
	glCallList(pawn);
	glPopMatrix();
}

void drawPieces()
{

	double thisTime = 20 * GetTime();

	// white pieces.
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, redMaterial);

	glPushMatrix();
	glTranslatef(rookLeftW.x, 0, rookLeftW.z);
	glCallList(rook);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(knightLeftW.x, 0, knightLeftW.z);
	glCallList(knight);
	glPopMatrix();

	Interpolate(thisTime, 80, 100, bishopLeftW.x, bishopLeftW.x, 6000);
	Interpolate(thisTime, 80, 100, bishopLeftW.z, bishopLeftW.z, 4000);
	glPushMatrix();
	glTranslatef(bishopLeftW.x, 0, bishopLeftW.z);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(kingW.x, 0, kingW.z);
	glCallList(king);
	glPopMatrix();

	Interpolate(thisTime, 140, 160, queenW.x, queenW.x, 3000);
	Interpolate(thisTime, 140, 160, queenW.z, queenW.z, 3000);
	Interpolate(thisTime, 200, 220, queenW.x, queenW.x, 3000);
	Interpolate(thisTime, 200, 220, queenW.z, queenW.z, 7000);
	glPushMatrix();
	glTranslatef(queenW.x, 0, queenW.z);
	glCallList(queen);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(bishopRightW.x, 0, bishopRightW.z);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(knightRightW.x, 0, knightRightW.z);
	glCallList(knight);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(rookRightW.x, 0, rookRightW.z);
	glCallList(rook);
	glPopMatrix();

	drawWhitePawns(thisTime);


	// black pieces
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, blueMaterial);

	glPushMatrix();
	glTranslatef(rookLeftB.x, 0, rookLeftB.z);
	glCallList(rook);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(knightLeftB.x, 0, knightLeftB.z);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(bishopLeftB.x, 0, bishopLeftB.z);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(kingB.x, 0, kingB.z);
	glCallList(king);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(queenB.x, 0, queenB.z);
	glCallList(queen);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(bishopRightB.x, 0, bishopRightB.z);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	Interpolate(thisTime, 110, 130, knightRightB.x, knightRightB.x, 8000);
	Interpolate(thisTime, 110, 130, knightRightB.z, knightRightB.z, 6000);
	glTranslatef(knightRightB.x, 0, knightRightB.z);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(rookRightB.x, 0, rookRightB.z);
	Interpolate(thisTime, 10, 30, rookRightB.x, rookRightB.x, 8000);
	glCallList(rook);
	glPopMatrix();

	drawBlackPawns(thisTime);

}

void drawSpaces()
{
	// draw board
	bool evenSquare = true;

	for (int i = 500; i <= 7500; i += 1000)
	{
		for (int j = 500; j <= 7500; j += 1000)
		{
			if (evenSquare)
			{
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, whiteMaterial);
				glBegin(GL_QUADS);
				glVertex3d(i, 0, j);
				glVertex3d(i, 0, j + 1000);
				glVertex3d(i + 1000, 0, j + 1000);
				glVertex3d(i + 1000, 0, j);
			}

			evenSquare = !evenSquare;
		}

		evenSquare = !evenSquare;
	}

	glEnd();
}


void drawBoard()
{
	// top
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, greenMaterial);
	glBegin(GL_QUADS);
	glVertex3d(0, -1, 0);
	glVertex3d(0, -1, 9000);
	glVertex3d(9000, -1, 9000);
	glVertex3d(9000, -1, 0);
	glEnd();

	// bottom
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, brightGreenMaterial);
	glBegin(GL_QUADS);
	glVertex3d(0, -1000, 0);
	glVertex3d(0, -1000, 9000);
	glVertex3d(9000, -1000, 9000);
	glVertex3d(9000, -1000, 0);
	glEnd();

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, darkGrey);
	glBegin(GL_QUADS);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, 9000);
	glVertex3d(0, -1000, 9000);
	glVertex3d(0, -1000, 0);
	glEnd();

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, darkGrey);
	glBegin(GL_QUADS);
	glVertex3d(0, 0, 9000);
	glVertex3d(9000, 0, 9000);
	glVertex3d(9000, -1000, 9000);
	glVertex3d(0, -1000, 9000);
	glEnd();

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, darkGrey);
	glBegin(GL_QUADS);
	glVertex3d(9000, 0, 9000);
	glVertex3d(9000, 0, 0);
	glVertex3d(9000, -1000, 0);
	glVertex3d(9000, -1000, 9000);
	glEnd();

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, darkGrey);
	glBegin(GL_QUADS);
	glVertex3d(9000, 0, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, -1000, 0);
	glVertex3d(9000, -1000, 0);
	glEnd();
}



// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	double t = GetTime();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	gluLookAt(eye[0], eye[1], eye[2],  at[0], at[1], at[2],  0,1,0); // Y is up!

	drawPieces();
	drawSpaces();
	drawBoard();

	GLfloat light_position[] = {1,3,-.1f, .25}; // light comes FROM this vector direction.
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); // position first light

	glutSwapBuffers();
	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
		case 27: // escape character means to quit the program
			exit(0);
			break;
		default:
			return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}



void SetPerspectiveView(int w, int h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	double aspectRatio = (GLdouble) w/(GLdouble) h;
	gluPerspective( 
	/* field of view in degree */ 45.0,
	/* aspect ratio */ aspectRatio,
	/* Z near */ 100, /* Z far */ 30000.0);
	glMatrixMode(GL_MODELVIEW);
}

// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	SetPerspectiveView(w,h);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	glutPostRedisplay();
}

// Your initialization code goes here.
void InitializeMyStuff()
{
	// set material's specular properties
	GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat mat_shininess[] = {100.0};
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	// set light properties
	GLfloat light_position[] = {(float)eye[0], (float)eye[1], (float)eye[2],1};
	GLfloat white_light[] = {1,1,1,1};
	GLfloat low_light[] = {.1f,.2f,1,1};
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); // position first light
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light); // specify first light's color
	glLightfv(GL_LIGHT0, GL_SPECULAR, low_light);

	glEnable(GL_DEPTH_TEST); // turn on depth buffering
	glEnable(GL_LIGHTING);	// enable general lighting
	glEnable(GL_LIGHT0);	// enable the first light.

	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(pawn, GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();
	glNewList(rook, GL_COMPILE);
	DrawPiece("ROOK.POL");
	glEndList();
	glNewList(knight, GL_COMPILE);
	DrawPiece("KNIGHT.POL");
	glEndList();
	glNewList(bishop, GL_COMPILE);
	DrawPiece("BISHOP.POL");
	glEndList();
	glNewList(king, GL_COMPILE);
	DrawPiece("KING.POL");
	glEndList();
	glNewList(queen, GL_COMPILE);
	DrawPiece("QUEEN.POL");
	glEndList();
	glNewList(bishop, GL_COMPILE);
	DrawPiece("BISHOP.POL");
	glEndList();
	glNewList(knight, GL_COMPILE);
	DrawPiece("KNIGHT.POL");
	glEndList();
	glNewList(rook, GL_COMPILE);
	DrawPiece("ROOK.POL");
	glEndList();
	glNewList(rook, GL_COMPILE);
	DrawPiece("ROOK.POL");
	glEndList();
	glNewList(knight, GL_COMPILE);
	DrawPiece("KNIGHT.POL");
	glEndList();
	glNewList(bishop, GL_COMPILE);
	DrawPiece("BISHOP.POL");
	glEndList();
	glNewList(king, GL_COMPILE);
	DrawPiece("KING.POL");
	glEndList();
	glNewList(queen, GL_COMPILE);
	DrawPiece("QUEEN.POL");
	glEndList();
	glNewList(bishop, GL_COMPILE);
	DrawPiece("BISHOP.POL");
	glEndList();
	glNewList(knight, GL_COMPILE);
	DrawPiece("KNIGHT.POL");
	glEndList();
	glNewList(rook, GL_COMPILE);
	DrawPiece("ROOK.POL");
	glEndList();

}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(10, 10);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Shapes");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);

	glClearColor(1,1,1,1);	
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
