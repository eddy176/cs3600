#pragma once
#ifndef PIECE_H
#define PIECE_H


class Piece
{
public:
	Piece(double x, double y, double z);
	double x;
	double y;
	double z;
	double deg = 0;
};


#endif