#include <iostream>
#include "glut.h"
#include "mouse.h"
#include "maze.h"
#include "view.h"
#include <cmath>
#include <ctime>
#include "graphics.h"

extern Maze gMaze;
const double SPIN_SPEED = 0.08;
const double MOVE_SPEED = 0.0022;


Mouse::Mouse()
{
	x = gMaze.mStart + 0.5;
	std::cout << "Start " <<gMaze.mStart << std::endl;
	std::cout << "End " << gMaze.mEnd << std::endl;
	y = 0.5;
	direction = 0;
	size = 0.15;
	xSpeed = 1;
	ySpeed = 1;
	super = false;
}


void Mouse::Draw()
{
	if (current_view == rat_view)
	{
		return; // drawing yourself in rat view looks bad.
	}
	else
	{
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		double squareSize = 0.3;
		glPushMatrix();
		glTranslated(x, y, 0);
		glRotated(direction, 0, 0, 1);

		drawCube(size);
		//DrawRectangle(5, 5, 5, 5);
	}


	glPopMatrix();
}
void Mouse::drawCube(double size)
{
	glBegin(GL_QUADS);

	// front wall
	glColor3d(0.63, 0.95, 0.04);
	glVertex3d(-size, size, 0);
	glVertex3d(size, size, 0);
	glVertex3d(size, size, size * 2);
	glVertex3d(-size, size, size * 2);
	// back wall
	glColor3d(0.69, 0.61, 0.65);
	glVertex3d(-size, -size, 0);
	glVertex3d(size, -size, 0);
	glVertex3d(size, -size, size * 2);
	glVertex3d(-size, -size, size * 2);
	// left wall
	glColor3d(0.62, 0.65, 0.64);
	glVertex3d(-size, -size, 0);
	glVertex3d(-size, size, 0);
	glVertex3d(-size, size, size * 2);
	glVertex3d(-size, -size, size * 2);
	// right wall (face)
	glColor3d(0.35, 0.55, 0.15);
	glVertex3d(size, size, 0);
	glVertex3d(size, -size, 0);
	glVertex3d(size, -size, size * 2);
	glVertex3d(size, size, size * 2);
	// top wall
	glColor3d(0.30, 0.31, 0.94);
	glVertex3d(-size, size, size * 2);
	glVertex3d(size, size, size * 2);
	glVertex3d(size, -size, size * 2);
	glVertex3d(-size, -size, size * 2);
	// bottom wall
	glColor3d(0.72, 0.62, 0.98);
	glVertex3d(-size, size, 0);
	glVertex3d(size, size, 0);
	glVertex3d(size, -size, 0);
	glVertex3d(-size, -size, 0);

	glEnd();
}
void Mouse::DrawRectangle(double x1, double y1, double x2, double y2)
{
	glBegin(GL_QUADS);
	glColor3ub(100, 62, 198);
	glVertex2d(x1, y1);
	glVertex2d(x2, y1);
	glVertex2d(x2, y2);
	glVertex2d(x1, y2);
	glEnd();
}

void Mouse::bob()
{

}
void Mouse::moveToStart()
{
	x = gMaze.mStart + 0.5;
	y = 0.5;
}

void Mouse::moveToEnd()
{
	x = gMaze.mEnd + 0.5;
	y = HEIGHT - 0.5;
}

void Mouse::moveToRandom()
{
	x = rand() % HEIGHT + 0.5;
	y = rand() % WIDTH + 0.5;
}

void Mouse::spinLeft()
{
	direction += SPIN_SPEED;
	double radians = direction / 180 * 3.141592654;
	xSpeed += cos(radians);
	ySpeed += sin(radians);

	glutPostRedisplay();
}

void Mouse::spinRight()
{
	direction -= SPIN_SPEED;
	double radians = direction / 180 * 3.141592654;
	xSpeed += cos(radians);
	ySpeed += sin(radians);

	glutPostRedisplay();
}


void Mouse::moveForward()
{
	double radians = direction / 180 * 3.141592654;
	xSpeed = cos(radians);
	ySpeed = sin(radians);
	double nextX = x + xSpeed * MOVE_SPEED;
	double nextY = y + ySpeed * MOVE_SPEED;

	if (!super) {
		if (gMaze.isSafe(nextX, nextY, size))
		{
			x += xSpeed * MOVE_SPEED;
			y += ySpeed * MOVE_SPEED;
		}
	}
	else if (super) {
		x += xSpeed * MOVE_SPEED*2;
		y += ySpeed * MOVE_SPEED * 2;
	}

	else
	{
		std::cout << "COLLISION" << std::endl;
	}

	glutPostRedisplay();
}

void Mouse::moveBack()
{
	double radians = direction / 180 * 3.141592654;
	xSpeed = cos(radians);
	ySpeed = sin(radians);
	double nextX = x - xSpeed * MOVE_SPEED;
	double nextY = y - ySpeed * MOVE_SPEED;

	if (!super) {
		if (gMaze.isSafe(nextX, nextY, size))
		{
			x -= xSpeed * MOVE_SPEED;
			y -= ySpeed * MOVE_SPEED;
		}
	}
	else if (super) {
		x -= xSpeed * MOVE_SPEED*2;
		y -= ySpeed * MOVE_SPEED*2;
	}


	glutPostRedisplay();
}

double Mouse::getX()
{
	return x;
}

double Mouse::getY()
{
	return y;
}

double Mouse::getSize()
{
	return size;
}

double Mouse::getDirection()
{
	return direction;
}

double Mouse::getXSpeed()
{
	return xSpeed;
}

double Mouse::getYSpeed()
{
	return ySpeed;
}
double Mouse::GetTime()
{
	static clock_t start_time = clock();
	clock_t current_time = clock();
	double t = double(current_time - start_time) / CLOCKS_PER_SEC;
	return (sin(t)+1)/2;
}
