#pragma once

const int WIDTH = 6;
const int HEIGHT = 5;

struct Cell
{
	Cell();
	void Draw(int x, int y);
	void Draw2d(int x, int y);
	void Draw3d(int x, int y);
	bool left, top, right, bottom;
	bool visited;
};

class Maze
{
public:
	int mStart;
	int mEnd;
	Maze();
	void RemoveWalls();
	void RemoveWallsR(int i, int j);
	bool isSafe(double x, double y, double radius);
	void Draw();

private:
	Cell cells[WIDTH][HEIGHT];


};