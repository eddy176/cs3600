class Mouse
{
public:
	Mouse();
	void Draw();
	void bob();
	bool super;
	void drawCube(double size);
	void DrawRectangle(double x1, double y1, double x2, double y2);
	void spinLeft();
	void spinRight();
	void moveForward();
	void moveBack();
	void moveToStart();
	void moveToEnd();
	void moveToRandom();
	double getX();
	double getY();
	double getSize();
	double getDirection();
	double getXSpeed();
	double getYSpeed();
	double x, y;  // in cell coords
	double size;
	double GetTime();
private:
	double direction;  // in degrees

	double xSpeed, ySpeed;

	
};
