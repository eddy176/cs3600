#include "Circle.h"
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <iostream>

Circle::Circle()
{
}

struct vectortype
{
	double x;
	double y;
};

void Circle::collideCheck(std::vector<Circle>& gCircles, size_t i)
{
	for (size_t j = i + 1; j < gCircles.size(); j++)
	{
		double x1 = this->getNextX();
		double y1 = this->getNextY();
		double x2 = gCircles[j].getNextX();
		double y2 = gCircles[j].getNextY();

		double distance = std::sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));

		if (distance < this->mRadius + gCircles[j].mRadius)
		{
			double rtemp, btemp, gtemp;
			rtemp = gCircles[i].mRed;
			gCircles[i].mRed = gCircles[j].mRed;
			gCircles[j].mRed = rtemp;

			btemp = gCircles[i].mBlue;
			gCircles[i].mBlue = gCircles[j].mBlue;
			gCircles[j].mBlue = btemp;

			gtemp = gCircles[i].mGreen;
			gCircles[i].mGreen = gCircles[j].mGreen;
			gCircles[j].mGreen = gtemp;

			this->collide(i, j, gCircles);
		}
	}
}


void Circle::collide(int p1, int p2, std::vector<Circle>& particles)
{
	double COLLISION_FRICTION = 1.0;

	vectortype en; // Center of Mass coordinate system, normal component
	vectortype et; // Center of Mass coordinate system, tangential component
	vectortype u[2]; // initial velocities of two particles
	vectortype um[2]; // initial velocities in Center of Mass coordinates
	double umt[2]; // initial velocities in Center of Mass coordinates, tangent component
	double umn[2]; // initial velocities in Center of Mass coordinates, normal component
	vectortype v[2]; // final velocities of two particles
	double m[2];    // mass of two particles
	double M; // mass of two particles together
	vectortype V; // velocity of two particles together
	double size;
	int i;

	double xdif = particles[p1].getNextX() - particles[p2].getNextX();
	double ydif = particles[p1].getNextY() - particles[p2].getNextY();

	// set Center of Mass coordinate system
	size = sqrt(xdif * xdif + ydif * ydif);
	xdif /= size; ydif /= size; // normalize
	en.x = xdif;
	en.y = ydif;
	et.x = ydif;
	et.y = -xdif;

	// set u values
	u[0].x = particles[p1].getXSpeed();
	u[0].y = particles[p1].getYSpeed();
	m[0] = particles[p1].getRadius() * particles[p1].getRadius();
	u[1].x = particles[p2].getXSpeed();
	u[1].y = particles[p2].getYSpeed();
	m[1] = particles[p2].getRadius() * particles[p2].getRadius();

	// set M and V
	M = m[0] + m[1];
	V.x = (u[0].x * m[0] + u[1].x * m[1]) / M;
	V.y = (u[0].y * m[0] + u[1].y * m[1]) / M;

	// set um values
	um[0].x = m[1] / M * (u[0].x - u[1].x);
	um[0].y = m[1] / M * (u[0].y - u[1].y);
	um[1].x = m[0] / M * (u[1].x - u[0].x);
	um[1].y = m[0] / M * (u[1].y - u[0].y);

	// set umt and umn values
	for (i = 0;i < 2;i++)
	{
		umt[i] = um[i].x * et.x + um[i].y * et.y;
		umn[i] = um[i].x * en.x + um[i].y * en.y;
	}

	// set v values
	for (i = 0;i < 2;i++)
	{
		v[i].x = umt[i] * et.x - umn[i] * en.x + V.x;
		v[i].y = umt[i] * et.y - umn[i] * en.y + V.y;
	}

	// reset particle values
	particles[p1].setXSpeed(v[0].x * COLLISION_FRICTION);
	particles[p1].setYSpeed(v[0].y * COLLISION_FRICTION);
	particles[p2].setXSpeed(v[1].x * COLLISION_FRICTION);
	particles[p2].setYSpeed(v[1].y * COLLISION_FRICTION);

}

double Circle::getX() {
	return this->mX;
}
double Circle::getY() {
	return this->mY;
}
double Circle::getRadius() {
	return this->mRadius;
}
double Circle::getXSpeed() {
	return this->mXspeed;
}
double Circle::getYSpeed() {
	return this->mYspeed;
}
double Circle::getRed() {
	return this->mRed;
}
double Circle::getGreen() {
	return this->mGreen;
}
double Circle::getBlue() {
	return this->mBlue;
}
double Circle::getNextX() {
	return this->mX + this->mXspeed;
}
double Circle::getNextY() {
	return this->mY + this->mYspeed;
}

void Circle::setX(double val) {
	this->mX = val;
}
void Circle::setY(double val) {
	this->mY = val;
}
void Circle::setRadius(double val) {
	this->mRadius = val;
}
void Circle::setXSpeed(double val) {
	this->mXspeed = val;
}
void Circle::setYSpeed(double val) { this->mYspeed = val; }

void Circle::setRed(double val) {
	this->mRed = val;
}
void Circle::setGreen(double val) {
	this->mGreen = val;
}
void Circle::setBlue(double val) {
	this->mBlue = val;
}
