#pragma once
#ifndef __CIRCLE_H__
#define __CIRCLE_H__
#include <vector>
#include <stdlib.h>
class Circle
{
public:
	Circle();
	double getX();
	double getY();
	double getRadius();
	double getRed();
	double getGreen();
	double getBlue();
	double getXSpeed();
	double getYSpeed();
	double getNextX();
	double getNextY();
	void setX(double value);
	void setY(double value);
	void setRadius(double value);
	void setRed(double value);
	void setGreen(double value);
	void setBlue(double value);
	void setXSpeed(double value);
	void setYSpeed(double value);

	void collide(int p1, int p2, std::vector<Circle>& particles);
	void collideCheck(std::vector<Circle>& allCircles, size_t i);
protected:
	double mX, mY, mRadius, mXspeed, mYspeed, mRed, mGreen, mBlue;
};
#endif